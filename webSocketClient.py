# -*- coding: utf-8 -*-

import websockets
import asyncio
import uuid
import json
import sys
import os
import time
import re

from time import sleep

class WebSocketClient():

    def __init__(self):
        # list of topics to subscribe to
        self.topics = ["channel-points-channel-v1.167534648"]
        self.auth_token = "auth token"
        pass

    async def connect(self):
        '''
           Connecting to webSocket server
           websockets.client.connect returns a WebSocketClientProtocol, which is used to send and receive messages
        '''
        self.connection = await websockets.client.connect('wss://pubsub-edge.twitch.tv')
        if self.connection.open:
            print('Connection stablished. Client correcly connected')
            # Send greeting
            message = {"type": "LISTEN", "nonce": str(self.generate_nonce()), "data":{"topics": self.topics, "auth_token": self.auth_token}}
            json_message = json.dumps(message)
            await self.sendMessage(json_message)
            return self.connection
    
    def generate_nonce(self):
        '''Generate pseudo-random number and seconds since epoch (UTC).'''
        nonce = uuid.uuid1()
        oauth_nonce = nonce.hex
        return oauth_nonce

    async def sendMessage(self, message):
        '''Sending message to webSocket server'''
        await self.connection.send(message)

    async def receiveMessage(self, connection):
        '''Receiving all server messages and handling them'''
        while True:
            try:
                message = await connection.recv()
  
                if message.find("Song Request") == -1:
                    "This request is not a song request"
                else:

                    start = message.find("user_input", message.find("user_input") + 1)
                    end = message.find("status")

                    user_input = message[start:end]
       
                    front_trimmed = user_input.replace(user_input[:15],'')
                    back_trimmed= front_trimmed.split("\\", 1)
                    search_for = back_trimmed[0]

                    redeemed_by_start = message.find("display_name", message.find("display_name")) 
                    redeemed_by_end = message.find ("channel_id")

                    reedeemed_by_inbetween = message[redeemed_by_start:redeemed_by_end]

                    reedemed_front_trimmed = reedeemed_by_inbetween.replace(reedeemed_by_inbetween[:17],'')

                    ss = reedemed_front_trimmed.translate({ord(i): None for i in '\\"},'})

                    print ("User " + ss + " has made a song request.")
                    print("The request was for : " + search_for)

                    items = [ss, search_for]
      
                    counter=0
                    for item in items:
                        with open('file_'+str(counter)+'.txt', 'w', encoding="utf-8") as f:
                            f.write("%s\n" % item)
                            counter = counter + 1

                    sleep(10);
                    
                    if os.path.exists("file_0.txt"):
                      os.remove("file_0.txt")
                    else:
                      print("The file does not exist")

                    if os.path.exists("file_1.txt"):
                       os.remove("file_1.txt")
                    else:
                       print("The file does not exist")
                    
                  
            except websockets.exceptions.ConnectionClosed:
                print('Connection with server closed')
                break

    async def heartbeat(self, connection):
        '''
        Sending heartbeat to server every 1 minutes
        Ping - pong messages to verify/keep connection is alive
        '''
        while True:
            try:
                data_set = {"type": "PING"}
                json_request = json.dumps(data_set)
                await connection.send(json_request)
                await asyncio.sleep(60)
            except websockets.exceptions.ConnectionClosed:
                print('Connection with server closed')
                break
